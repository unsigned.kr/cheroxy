/**
 * CRXHttpRequest.h
 */
#ifndef __CRXHTTPREQUEST_H__
#define __CRXHTTPREQUEST_H__

#include "CRXHttpHeader.h"

class CRXHttpRequest : public CRXHttpHeader
{
private:
	std::string	mMethod;
	std::string	mUrl;
	std::string	mFileExtension;

	std::string	mProtocol;
	std::string	mHost;
	int			mPort;

public:
	CRXHttpRequest		(void);
	~CRXHttpRequest		(void);

private:
	void				Parse				(void);
	void				ParseFileExtension	(void);

public:
	void				SetHeader			(const char * aHttpRequest);
	std::string			GetHeader			(void) const;

public:
	std::string			GetURL				(void) const;
	std::string			GetHost				(void) const;
	int					GetPort				(void) const;
	std::string			GetFileExtension	(void) const;
};

#endif // #ifndef __CRXHTTPREQUEST_H__