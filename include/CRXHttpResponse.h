/**
 * CRXHttpResponse.h
 */
#ifndef __CRXHTTPRESPONSE_H__
#define __CRXHTTPRESPONSE_H__

#include "CRXHttpHeader.h"

#include <vector>

#define ERROR_HTTP_RESPONSE									-4000
#define ERROR_HTTP_RESPONSE_INVALID_LENGTH					ERROR_HTTP_RESPONSE - 1
#define ERROR_HTTP_RESPONSE_INVALID_FORMAT					ERROR_HTTP_RESPONSE - 2
#define ERROR_HTTP_RESPONSE_FAILED_TO_MEMORY_ALLOCATION		ERROR_HTTP_RESPONSE - 3
#define ERROR_HTTP_RESPONSE_FAILED_TO_PARSE_CONTENT			ERROR_HTTP_RESPONSE - 4

class CRXHttpResponse : public CRXHttpHeader
{
private:
	int					mStatusCode;
	std::string			mStatusString;
	bool				mIsChunked;		/* Transfer-Encoding */
	unsigned int		mContentLength;
	std::vector <char>	mContent;

public:
	CRXHttpResponse		(void);
	~CRXHttpResponse	(void);

private:
	void				Parse				(void);
	void				Reset				(void);

public:
	int					GetStatusCode		(void) const;

	bool				IsChunked			(void) const;
	void				SetHeader			(const char	* aHeader);
	std::string			GetHeader			(void) const;
	int					SetContent			(const char	* aContent,
											 const int	aLength);
	const char *		GetContentBody		(void) const;
	int					GetContentLength	(void) const;
};

#endif // #ifndef __CRXHTTPRESPONSE_H__
