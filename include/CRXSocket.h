/**
 * CRXSocket.h
 */
#ifndef __CRXSOCKET_H__
#define __CRXSOCKET_H__

#include "CRXException.h"

#ifdef WIN32
# include <WinSock2.h>
typedef int socklen_t;
#else
# include <netinet/in.h>
# include <sys/socket.h>
# include <arpa/inet.h>
# include <netdb.h>
#endif

#include <string>

#define TCPSOCKET_NO_TIMEOUT					-1

#define ERROR_TCPSOCKET							-1000

#define ERROR_TCPSOCKET_FAILED_TO_INITIALIZE	ERROR_TCPSOCKET - 1
#define ERROR_TCPSOCKET_ALREADY_IN_USE			ERROR_TCPSOCKET - 2
#define ERROR_TCPSOCKET_FAILED_TO_CREATE_SOCKET	ERROR_TCPSOCKET - 3
#define ERROR_TCPSOCKET_FAILED_TO_GET_HOSTNAME	ERROR_TCPSOCKET - 4
#define ERROR_TCPSOCKET_FAILED_TO_CONNECT		ERROR_TCPSOCKET - 5
#define ERROR_TCPSOCKET_FAILED_TO_BIND			ERROR_TCPSOCKET - 6
#define ERROR_TCPSOCKET_FAILED_TO_LISTEN		ERROR_TCPSOCKET - 7
#define ERROR_TCPSOCKET_FAILED_TO_ACCEPT		ERROR_TCPSOCKET - 8
#define ERROR_TCPSOCKET_FAILED_TO_SEND			ERROR_TCPSOCKET - 9
#define ERROR_TCPSOCKET_FAILED_TO_RECEIVE		ERROR_TCPSOCKET - 10
#define ERROR_TCPSOCKET_NOT_READY				ERROR_TCPSOCKET - 11
#define ERROR_TCPSOCKET_FAILED_TO_SET_SOCKOPT	ERROR_TCPSOCKET - 12
#define ERROR_TCPSOCKET_FAILED_TO_SET_TIMEOUT	ERROR_TCPSOCKET - 13
#define ERROR_TCPSOCKET_FAILED_TO_GET_SOCKOPT	ERROR_TCPSOCKET - 14

class CRXSocket : public CRXException
{
private:
	struct sockaddr_in	mAddress;
	int					mSocket;

private:
	/**
	 * create socket.
	 * @return success : socket id, failed : -1.
	 */
	int			Create			(void);

	/**
	 * check socket status.
	 * @return available : true, else : false.
	 */
	bool		IsCreated		(void) const;

	bool		IsReady			(void) const;

	/**
	 * static functions
	 */
	static int	Initialize		(void);

	static bool	IsInitialized	(void);

	static void	Finalize		(void);


public:
	/**
	 * common functions
	 */
	CRXSocket					(int aSocket = -1);

	~CRXSocket					(void);

	void		Attach			(int aSocket);

	int			Detach			(void);

	void		Close			(void);

				operator int	(void) const;

	CRXSocket	& operator =	(int aSocket);

	int			GetOption		(const int	aOptName,
								 void		* aOptVal,
								 size_t		* aOptLen);

	int			SetOption		(const int		aOptName,
								 const void		* aOptVal,
								 const size_t	aOptLen);

	int			SetTimeout		(const int aTimeout);

	/**
	 * for server
	 */
	int CreateServer			(const unsigned short	aPort,
								 const int				aBacklog = 5,
								 struct sockaddr_in		* aAddress = NULL);

	int Accept					(struct sockaddr_in		* aRemoteAddress = NULL,
								 int					* aAddressLength = NULL);

	/**
	 * for client
	 */
	int Connect					(const std::string		aUrl,
								 const unsigned short	aPort,
								 const int				aTimeout = TCPSOCKET_NO_TIMEOUT);

	/**
	 * for communication
	 */
	int Send					(const char	* aBuffer,
								 int		aSize);

	int Receive					(char	* aBuffer,
								 int	aSize);
};

#endif // #ifndef __CRXSOCKET_H__
