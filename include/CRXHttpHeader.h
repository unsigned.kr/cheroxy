/**
 * CRXHttpHeader.h
 */
#ifndef __CRXHTTPMESSAGE_H__
#define __CRXHTTPMESSAGE_H__

#include <string>

#define CRLF				"\r\n"
#define CRLF2				"\r\n\r\n"	

class CRXHttpHeader
{
protected:
	std::string	mHttpHeader;
	std::string	mHttpVersion;

public:
	CRXHttpHeader			(void);
	virtual ~CRXHttpHeader	(void);

protected:
	virtual void	Parse	(void) = 0;

public:
	void			Set		(const char * aHeader);
	std::string		Get		(void) const;
};

#endif // #ifndef __CRXHTTPMESSAGE_H__
