/*
 * CRXMutex.h
 */

#ifdef _WIN32
# include <Windows.h>
# define CRX_MUTEX	HANDLE
#else
# include <pthread.h>
# define CRX_MUTEX	pthread_mutex_t
#endif

class CRXMutex
{
private:
	bool		mIsCreated;
	CRX_MUTEX	mMutex;

public:
	CRXMutex			(void);
	~CRXMutex			(void);

public:
	bool	IsCreated	(void) const;
	int		Create		(void);
	int		Destroy		(void);
	void	Lock		(void);
	void	Unlock		(void);
};
