/**
 * CRXFilter.h
 */

#ifndef __CRXFILTER_H__
#define __CRXFILTER_H__

#include "CRXHttpRequest.h"
#include "CRXHttpResponse.h"

#include <string>
#include <list>

#define FILTER_DELIMITER	'|'

#define CRX_FILTER_MATCHED			true
#define CRX_FILTER_NOT_MATCHED		false

typedef enum
{
	CRX_FILTER_REQUEST_FILE_EXTENSION = 1
} E_CRX_FILTER_REQUEST;

class CRXFilter
{
private:
	typedef struct __crx_filter__ {
		E_CRX_FILTER_REQUEST	mFilterType;
		bool					mIsMatched;
		std::string				mValue;
	} S_CRX_FILTER;

	typedef std::list<S_CRX_FILTER>		CRXFilter_t;

	CRXFilter_t		mRequestFilterList;
	CRXFilter_t		mResponseFilterList;

public:
	CRXFilter	(void);
	~CRXFilter	(void);

public:
	void		SetRequestFilter		(const E_CRX_FILTER_REQUEST	aType,
										 const bool					aIsMatched,
										 const std::string			aValue);

	void		RemoveRequestFilter		(const E_CRX_FILTER_REQUEST	aType);

	bool		CheckRequestFilter		(const E_CRX_FILTER_REQUEST	aType,
										 const CRXHttpRequest		& aRequest);
};

#endif // #ifndef __CRXFILTER_H__