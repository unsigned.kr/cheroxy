/**
 * CRXException.h
 */
#ifndef __CRXEXCEPTION_H__
#define __CRXEXCEPTION_H__

#include <string>
#include <stdarg.h>

#ifdef _WIN32
# define __func__			__FUNCTION__
#endif

#ifdef _DEBUG
#include <stdio.h>
#include <ctype.h>
#define CRX_DEBUG_TRACE(FMT, ...)													\
	do{																				\
		fprintf(stderr,"[DEBUG][%s %s(%d)] ",__FILE__,__func__,__LINE__);			\
		fprintf(stderr,FMT,##__VA_ARGS__);											\
}while(0)
#define CRX_DEBUG_TRACE_BIN(__VAL,__LEN,FMT,...)									\
	CRX_DEBUG_TRACE(FMT,##__VA_ARGS__);												\
	do{																				\
		int __i, __j;																\
		unsigned char * __U_V = (unsigned char *)__VAL;								\
		for (__i=0 ; __i<__LEN; __i+=16)											\
		{																			\
			fprintf(stderr, "[DEBUG][%s %s(%d)] ",__FILE__,__func__,__LINE__);		\
			fprintf(stderr, "%06x : ", __i);										\
			for (__j=0 ; __j<16 ; __j++)											\
			{																		\
				if (__i+__j < __LEN)												\
					fprintf(stderr, "%02x ", __U_V[__i+__j]);						\
				else																\
					fprintf(stderr, "   ");											\
			}																		\
			fprintf(stderr," ");													\
			for (__j=0 ; __j<16 ; __j++)											\
			{																		\
				if (__i+__j < __LEN)												\
					fprintf(stderr,"%c",											\
							' ' <= __U_V[__i+__j] && __U_V[__i+__j] <= '~' ? __U_V[__i+__j] : '.');		\
			}																		\
			fprintf(stderr,"\n");													\
		}																			\
	}while(0)
#endif

#define CRX_ERROR_SET(__CODE,__FMT,...)										\
	do {																	\
		SetErrorCode (__CODE);												\
		SetErrorMessage (__FILE__,__func__,__LINE__,__FMT,##__VA_ARGS__);	\
	} while (0)

#define CRX_ADD_SUBCLASS_ERROR(__SUB_CLASS)									\
	do {																	\
		AddMessage (__SUB_CLASS.GetErrorMessage ().c_str ());				\
	} while (0)


#define CRX_SYSTEM_ERROR()	CRXException::GetSystemErrorCode ()

class CRXException
{
private:
	std::string		mMessage;
	int				mCode;

public:
	CRXException			(void);
	virtual ~CRXException	(void);

public:
	static int	GetSystemErrorCode		(void);

public:
	/* get */
	int			GetErrorCode			(void) const;

	std::string	GetErrorMessage			(void) const;

	/* set */
	void		SetErrorCode			(const int	aCode);

	void		SetErrorMessage			(const char	* aFile,
										 const char	* aFunction,
										 const int	aLine,
										 const char	* aFormat, ...);

	std::string	MakeErrorMessage		(const char	* aFile,
										 const char	* aFunction,
										 const int	aLine,
										 const char	* aFormat,
										 va_list	aVaList);

	void		AddMessage				(const char * aMessage);
};

#endif // #ifndef __CRXEXCEPTION_H__
