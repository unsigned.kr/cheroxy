/**
 * CRXProxy.h
 */
#ifndef __CRXPROXY_H__
#define __CRXPROXY_H__

#include "CRXException.h"
#include "CRXSocket.h"
#include "CRXHttpHeader.h"
#include "CRXHttpRequest.h"
#include "CRXHttpResponse.h"
#include "CRXFilter.h"

#define ERROR_PROXY								-10000
#define ERROR_PROXY_FAILED_TO_RECEIVE_REQUEST	ERROR_PROXY - 1
#define ERROR_PROXY_FAILED_TO_SEND_REQUEST		ERROR_PROXY - 2
#define ERROR_PROXY_FAILED_TO_RECEIVE_RESPONSE	ERROR_PROXY - 3
#define ERROR_PROXY_FAILED_TO_SEND_RESPONSE		ERROR_PROXY - 4
#define ERROR_PROXY_FAILED_TO_CONNECT_TO_SERVER	ERROR_PROXY - 5
#define ERROR_PROXY_FAILED_TO_SET_RESPONSE		ERROR_PROXY - 6
#define ERROR_PROXY_FAILED_TO_ACCEPT_CLIENT		ERROR_PROXY - 7


class CRXProxy : public CRXException
{
private:
	CRXSocket			mClient;
	CRXSocket			mServer;

	int					mServerTimeout;

	CRXHttpRequest		mHttpRequest;
	CRXHttpResponse		mHttpResponse;

	static CRXFilter	mFilter;

public:
	CRXProxy			(const int aSocket);
	~CRXProxy			(void);

public:
	/* common utilities */
	void	SetServerTimeout			(const int	aTimeout = TCPSOCKET_NO_TIMEOUT);
	void	Close						(void);

public:
	/* for communication */
	int		Forward						(void);

	int		ReceiveRequest				(void);
	int		SendRequest					(void);

	int		ReceiveResponse				(void);
	int		SendResponse				(void);

public:
	/* HTTP data */
	CRXFilter &			GetFilter		(void) const;
	CRXHttpRequest &	GetHttpRequest	(void);
	CRXHttpResponse &	GetHttpResponse	(void);
};

#endif // #ifndef __CRXPROXY_H__
