/**
 * CRXSocket.cpp
 */
#include "CRXSocket.h"

#include <string.h>

#ifdef _WIN32
# pragma comment (lib, "ws2_32.lib")
# define close(__socket) closesocket(__socket)
#endif

bool gInitialized = false;

CRXSocket::CRXSocket (int aSocket)
{
	/*----------------------------------------------------------------*/
	CRXSocket::Initialize ();

	memset ((void *)&mAddress, 0x0, sizeof (struct sockaddr_in));

	Attach (aSocket);
	/*----------------------------------------------------------------*/
}

CRXSocket::~CRXSocket (void)
{
	/*----------------------------------------------------------------*/
	Close ();
	/*----------------------------------------------------------------*/
}

void
CRXSocket::Attach (int aSocket)
{
	int		aResult = 0;
	int		aReuseAddress = 0;
	size_t	aReuseAddressLength = sizeof (aReuseAddress);

	/*----------------------------------------------------------------*/
	mSocket = aSocket;

	if (mSocket < 0)
		return ;

	aResult = GetOption (SO_REUSEADDR,
						 &aReuseAddress,
						 &aReuseAddressLength);
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_GET_SOCKOPT;
		CRX_ERROR_SET (aResult, "Failed to get socket option (reuseaddr).(%d)", mSocket);
	}

	if (aReuseAddress == 1)
		return ;

	aReuseAddress = 1;

	aResult = SetOption (SO_REUSEADDR,
						 &aReuseAddress,
						 sizeof (aReuseAddress));
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_SET_SOCKOPT;
		CRX_ERROR_SET (aResult, "Failed to set socket option (reuseaddr).(%d)", mSocket);
	}
	/*----------------------------------------------------------------*/
}

int
CRXSocket::Detach (void)
{
	int aSocket = mSocket;

	/*----------------------------------------------------------------*/
	mSocket = 0;
	/*----------------------------------------------------------------*/
	return aSocket;
}

void
CRXSocket::Close (void)
{
	/*----------------------------------------------------------------*/
	if (mSocket <= 0)
		return ;

	close (mSocket);
	mSocket = 0;
	/*----------------------------------------------------------------*/
}

CRXSocket::operator int (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
	return mSocket;
}

CRXSocket &
CRXSocket::operator = (int aSocket)
{
	/*----------------------------------------------------------------*/
	Attach (aSocket);
	/*----------------------------------------------------------------*/
	return *this;
}

int
CRXSocket::GetOption (const int	aOptName,
					  void		* aOptVal,
					  size_t	* aOptLen)
{
	int	aResult = 0;

	/*----------------------------------------------------------------*/
	aResult = getsockopt (mSocket,
						  SOL_SOCKET,
						  aOptName,
#ifdef _WIN32
						  (char *) aOptVal,
#else
						  aOptVal,
#endif
						  (socklen_t *) aOptLen);
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_GET_SOCKOPT;
		CRX_ERROR_SET (aResult, "Failed to get socket option.");
	}
	/*----------------------------------------------------------------*/

	return aResult;
}

int
CRXSocket::SetOption (const int		aOptName,
					  const void	* aOptVal,
					  const size_t	aOptLen)
{
	int	aResult = 0;

	/*----------------------------------------------------------------*/
	aResult = setsockopt (mSocket,
						  SOL_SOCKET,
						  aOptName,
#ifdef _WIN32
						  (char *) aOptVal,
#else
						  aOptVal,
#endif
						  (socklen_t) aOptLen);
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_SET_SOCKOPT;
		CRX_ERROR_SET (aResult, "Failed to set socket option.");
	}
	/*----------------------------------------------------------------*/

	return aResult;
}

int
CRXSocket::SetTimeout (const int aTimeout)
{
	int				aResult = 0;

#ifndef _WIN32
	struct timeval	aTimeVal;
	aTimeVal.tv_sec = aTimeout;
	aTimeVal.tv_usec= 0;
#else
	int				aTimeMilliSec = aTimeout * 1000;
#endif

	/*----------------------------------------------------------------*/
	if (!IsReady ())
		return ERROR_TCPSOCKET_NOT_READY;

	if (aTimeout == TCPSOCKET_NO_TIMEOUT)
		return 0;

	aResult = SetOption (SO_RCVTIMEO,
#ifdef _WIN32
						 &aTimeMilliSec,
						 sizeof (aTimeMilliSec));
#else
						 &aTimeVal,
						 sizeof (aTimeVal));
#endif
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_SET_TIMEOUT;
		CRX_ERROR_SET (aResult, "Failed to set socket tieout.");
	}
	/*----------------------------------------------------------------*/

	return aResult;
}

int
CRXSocket::Initialize (void)
{
	int aResult = 0;

	/*----------------------------------------------------------------*/
	if (CRXSocket::IsInitialized ())
		return aResult;

#ifdef WIN32
	WSADATA aWinSockData;
	
	aResult = WSAStartup (MAKEWORD (2, 0), &aWinSockData);
	if (aResult != 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_INITIALIZE;
		return aResult;
	}
#endif

	gInitialized = true;

	/*----------------------------------------------------------------*/
	return aResult;
}

void
CRXSocket::Finalize (void)
{
	/*----------------------------------------------------------------*/
#ifdef WIN32
	WSACleanup ();
#endif
	/*----------------------------------------------------------------*/
}

bool
CRXSocket::IsInitialized (void)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
	return gInitialized;
}

bool
CRXSocket::IsReady (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
	return IsInitialized () && IsCreated ();
}

bool
CRXSocket::IsCreated (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
	return mSocket > 0 ? true : false;
}

int
CRXSocket::Create (void)
{
	int aResult = -1;
	int aReuseAddress = 1;

	/*----------------------------------------------------------------*/
	if (!IsInitialized () || IsCreated ())
	{
		aResult = ERROR_TCPSOCKET_ALREADY_IN_USE;
		CRX_ERROR_SET (aResult, "Already in use.");
		return aResult;
	}

	aResult = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_CREATE_SOCKET;
		CRX_ERROR_SET (aResult, "Failed to create socket(%d).", CRX_SYSTEM_ERROR ());
		return aResult;
	}

	mSocket = aResult;

	aResult = SetOption (SO_REUSEADDR,
						 &aReuseAddress,
						 sizeof (aReuseAddress));
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_SET_SOCKOPT;
		CRX_ERROR_SET (aResult, "Failed to set socket option (reuseaddr).");
	}
	/*----------------------------------------------------------------*/

	return aResult;
}

int
CRXSocket::Connect (const std::string		aUrl,
					const unsigned short	aPort,
					const int				aTimeout)
{
	int				aResult = -1;
	struct hostent	* aHostEnt;
	struct linger	aLinger;

	/*----------------------------------------------------------------*/
	aResult = Create ();
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_CREATE_SOCKET;
		CRX_ERROR_SET (aResult, "Failed to create socket(%d).", CRX_SYSTEM_ERROR ());
		return aResult;
	}

	mAddress.sin_family			= AF_INET;
	mAddress.sin_port			= htons (aPort);
	mAddress.sin_addr.s_addr	= inet_addr (aUrl.c_str ());

	if (mAddress.sin_addr.s_addr == (unsigned int)-1)
	{
		aHostEnt = gethostbyname (aUrl.c_str ());
		if (aHostEnt == NULL)
		{
			aResult = ERROR_TCPSOCKET_FAILED_TO_GET_HOSTNAME;
			CRX_ERROR_SET (aResult, "Failed to get hostname.");
			return aResult;
		}
		mAddress.sin_family = aHostEnt->h_addrtype;
		memcpy (&(mAddress.sin_addr.s_addr), aHostEnt->h_addr, aHostEnt->h_length);
	}

	aLinger.l_onoff = 1;
	aLinger.l_linger = 0;
	aResult = SetOption (SO_LINGER,
						 &aLinger,
						 sizeof (aLinger));
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_SET_SOCKOPT;
		CRX_ERROR_SET (aResult, "Failed to set socket option (linger).");
	}

	aResult = SetTimeout (aTimeout);
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_SET_TIMEOUT;
		CRX_ERROR_SET (aResult, "Failed to set timeout (%d).", CRX_SYSTEM_ERROR ());
		return aResult;
	}

	aResult = connect (mSocket, (struct sockaddr*) &mAddress, sizeof (mAddress));
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_CONNECT;
		CRX_ERROR_SET (aResult, "Failed to connect (%d).", CRX_SYSTEM_ERROR ());
	}
	/*----------------------------------------------------------------*/

	return aResult;
}

int
CRXSocket::CreateServer (const unsigned short	aPort,
						 const int				aBacklog,
						 struct sockaddr_in		* aAddress)
{
	int aResult = -1;

	/*----------------------------------------------------------------*/
	aResult = Create ();
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_CREATE_SOCKET;
		CRX_ERROR_SET (aResult, "Failed to create socket(%d).", CRX_SYSTEM_ERROR ());
		return aResult;
	}

	mAddress.sin_family		= AF_INET;
	mAddress.sin_addr.s_addr= htonl (INADDR_ANY);
	mAddress.sin_port		= htons (aPort);

	aResult = bind (mSocket, (struct sockaddr *)&mAddress, sizeof (struct sockaddr));
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_BIND;
		CRX_ERROR_SET (aResult, "Failed to bind(%d).", CRX_SYSTEM_ERROR ());
		return aResult;
	}

	aResult = listen (mSocket, aBacklog);
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_LISTEN;
		CRX_ERROR_SET (aResult, "Failed to listen(%d).", CRX_SYSTEM_ERROR ());
		return aResult;
	}

	if (aAddress != NULL)
	{
		memset ((void *)aAddress, 0x0, sizeof (struct sockaddr_in));
		memcpy ((void *)aAddress, (void *)&mAddress, sizeof (struct sockaddr_in));
	}
	/*----------------------------------------------------------------*/

	return aResult;
}

int
CRXSocket::Accept (struct sockaddr_in	* aRemoteAddress,
				   int					* aAddressLength)
{
	int					aResult = -1;

	struct sockaddr_in	aAddress;
	socklen_t			aLength = sizeof (aAddress);
	/*----------------------------------------------------------------*/
	if (!IsReady ())
		return ERROR_TCPSOCKET_NOT_READY;

	aResult = accept (mSocket, (struct sockaddr *) &aAddress, &aLength);
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_ACCEPT;
		CRX_ERROR_SET (aResult, "Failed to accept(%d).", CRX_SYSTEM_ERROR ());
		return aResult;
	}

	if (aRemoteAddress != NULL)
	{
		memset ((void *)aRemoteAddress, 0x0, sizeof (struct sockaddr_in));
		memcpy ((void *)aRemoteAddress, (void *)&aAddress, sizeof (struct sockaddr_in));
	}

	if (aAddressLength != NULL)
		*aAddressLength = aLength;
	/*----------------------------------------------------------------*/

	return aResult;
}

int
CRXSocket::Send (const char	* aBuffer,
				 int		aSize)
{
	int aResult = -1;

	/*----------------------------------------------------------------*/
	if (!IsReady ())
		return ERROR_TCPSOCKET_NOT_READY;

	aResult = send (mSocket, aBuffer, aSize, 0);
	if (aResult != aSize)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_SEND;
		CRX_ERROR_SET (aResult, "Failed to send(%d).", CRX_SYSTEM_ERROR ());
	}
	/*----------------------------------------------------------------*/

	return aResult;
}

int
CRXSocket::Receive (char	* aBuffer,
					int		aSize)
{
	int aResult = -1;

	/*----------------------------------------------------------------*/
	if (!IsReady ())
		return ERROR_TCPSOCKET_NOT_READY;

	aResult = recv (mSocket, aBuffer, aSize, 0);
	if (aResult < 0)
	{
		aResult = ERROR_TCPSOCKET_FAILED_TO_RECEIVE;
		CRX_ERROR_SET (aResult, "Failed to receive(%d).", CRX_SYSTEM_ERROR ());
	}
	/*----------------------------------------------------------------*/

	return aResult;
}
