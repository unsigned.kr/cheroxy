#ifdef _WIN32
# define _CRT_SECURE_NO_WARNINGS
#endif

#include "CRXSocket.h"
#include "CRXProxy.h"

#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

#ifdef _WIN32
# include <process.h>
# define THREAD_TYPE							void *
# define THREAD_FUNCTION_RETURN					unsigned int
# define THREAD_FUNCTION_CALLING_CONVENTION		WINAPI
#else
# include <pthread.h>
# define THREAD_TYPE							pthread_t
# define THREAD_FUNCTION_RETURN					void *
# define THREAD_FUNCTION_CALLING_CONVENTION
#endif

#define PROXY_CONNECTION	"Proxy-Connection"
#define CONNECTION			"Connection"

THREAD_FUNCTION_RETURN
THREAD_FUNCTION_CALLING_CONVENTION
CRXProxyMTWrapper (void * aThreadArg);

int ThreadPool (CRXSocket	* aProxySocket,
				const int	aThreadPoolCount);

bool	gViewRequest	= true;
bool	gViewResponse	= false;

int main (int argc, char* argv[])
{
	int			aResult	= 0;
	const short	aPort	= 8080;
	const int	aThreadPoolCount = 50;

	CRXSocket	aProxySocket;

	/*----------------------------------------------------------------*/
	/* 1. create proxy server */
	aResult = aProxySocket.CreateServer (aPort);
	if (aResult < 0)
	{
		cout << "Failed to create server." << endl;
		return aResult;
	}

	/* 2. ready proxy servers */
	aResult = ThreadPool (&aProxySocket, aThreadPoolCount);
	if (aResult)
	{
		cout << "Failed to create thread pool." << endl;
		return aResult;
	}

	/* 3. keep process */
	while (true);

	/*----------------------------------------------------------------*/
	aProxySocket.Close ();

	return aResult;
}

int ThreadPool (CRXSocket	* aProxySocket,
				const int	aThreadPoolCount)
{
	int				aResult = 0;
	int				aIter = 0;
	THREAD_TYPE		aThreadID = (THREAD_TYPE)0;

	/*----------------------------------------------------------------*/
	for (aIter = 0 ; aIter < aThreadPoolCount ; aIter++)
	{
#ifdef _WIN32
		aThreadID = (void *)_beginthreadex (0, 0, CRXProxyMTWrapper, aProxySocket, 0, 0);
		if (aThreadID == NULL)
		{
			aResult = -1;
			break;
		}
		CloseHandle (aThreadID);
#else
		aResult = pthread_create (&aThreadID, NULL, CRXProxyMTWrapper, aProxySocket);
		if (aResult < 0)
		{
			break;
		}
#endif
		cout << "Thread(" << aIter + 1 << ") is created successfully." << endl;
	}
	/*----------------------------------------------------------------*/

	return aResult;
}

THREAD_FUNCTION_RETURN
THREAD_FUNCTION_CALLING_CONVENTION
CRXProxyMTWrapper (void * aThreadArg)
{
	int			aResult = 0;

	CRXSocket	& aProxySocket = *((CRXSocket *)aThreadArg);
	CRXProxy	* aProxy = NULL;

	const char	aFilterFileExtension[] = "exe|gif|jpg|png|css|js|ico|swf|";

	/*----------------------------------------------------------------*/
	for (;;)
	{
		aResult = aProxySocket.Accept ();
		if (aResult < 0)
		{
			cout << aProxySocket.GetErrorMessage () << endl;
			aResult = -1;
			break;
		}

		aProxy = new(std::nothrow) CRXProxy (aResult);
		if (aProxy == NULL)
		{
			cout << "Failed to get proxy." << endl;
			aResult = -1;
			break;
		}
		aProxy->SetServerTimeout (2);

		CRXFilter		& aFilter	= aProxy->GetFilter ();
		CRXHttpRequest	& aRequest	= aProxy->GetHttpRequest ();
		CRXHttpResponse	& aResponse	= aProxy->GetHttpResponse ();

		aFilter.SetRequestFilter (CRX_FILTER_REQUEST_FILE_EXTENSION,
								  CRX_FILTER_MATCHED,
								  aFilterFileExtension);

		/* 1. receive request from client */
		aResult = aProxy->ReceiveRequest ();
		if (aResult < 0)
		{
			cout << aProxy->GetErrorMessage () << endl;
		}

		/* 1.1. modify and print request data */
		if (gViewRequest && !aFilter.CheckRequestFilter (CRX_FILTER_REQUEST_FILE_EXTENSION, aRequest))
		{
			string	aRequestHeader = aRequest.GetHeader ();
			size_t	aPositionProxyConnection = aRequestHeader.find (PROXY_CONNECTION);
			if (aPositionProxyConnection != string::npos)
			{
				aRequestHeader.replace (aPositionProxyConnection, strlen (PROXY_CONNECTION), CONNECTION);
				aRequest.SetHeader (aRequestHeader.c_str ());
			}

			CRX_DEBUG_TRACE ("== Request:  \n%s\n", aRequest.GetHeader ().c_str ());
		}

		/* 2. send request to web server */
		aResult = aProxy->SendRequest ();
		if (aResult < 0)
		{
			cout << aProxy->GetErrorMessage () << endl;
		}

		/* 3. receive response from web server */
		aResult = aProxy->ReceiveResponse ();
		if (aResult < 0)
		{
			cout << aProxy->GetErrorMessage () << endl;
		}

		/* 3.1. if request url is matched, print response */
		if (gViewResponse && !aFilter.CheckRequestFilter (CRX_FILTER_REQUEST_FILE_EXTENSION, aRequest))
		{
			CRX_DEBUG_TRACE ("== Response: \n%s\n", aResponse.GetHeader ().c_str ());
			CRX_DEBUG_TRACE_BIN (aResponse.GetContentBody (),
								 aResponse.GetContentLength (),
								 "== Content-Body: \n");
		}

		/* 4. send response to client */
		aResult = aProxy->SendResponse ();
		if (aResult < 0)
		{
			cout << aProxy->GetErrorMessage () << endl;
		}

		delete aProxy;
	}

	cout << " thread exit." << endl;
	/*----------------------------------------------------------------*/

	return (THREAD_FUNCTION_RETURN)0;
}
