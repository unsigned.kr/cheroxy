/**
 * CRXFilter.cpp
 */

#include "CRXFilter.h"

CRXFilter::CRXFilter (void)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
}

CRXFilter::~CRXFilter (void)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
}

void
CRXFilter::SetRequestFilter (const E_CRX_FILTER_REQUEST	aType,
							 const bool					aIsMatched,
							 const std::string			aValue)
{
	bool			aIsExistType = false;
	S_CRX_FILTER	aNewFilter;

	size_t			aPrevPosition = 0;
	size_t			aNextPosition = 0;
	std::string		aNewValue;

	CRXFilter_t::iterator	aIter = mRequestFilterList.begin ();

	/*----------------------------------------------------------------*/
	for ( ; aIter != mRequestFilterList.end () ; aIter++)
	{
		if (aIter->mFilterType == aType)
		{
			aIsExistType = true;
			break;
		}
	}

	if (aIsExistType)
	{
		aIter->mIsMatched	= aIsMatched;

		/* add only new value */
		for ( /* NO INIT */ ; aNextPosition != std::string::npos
							; aPrevPosition = aNextPosition + 1)
		{
			aNextPosition = aValue.find (FILTER_DELIMITER, aPrevPosition);
			aNewValue = aValue.substr (aPrevPosition, aNextPosition - aPrevPosition);
			if (aIter->mValue.find (aNewValue) == std::string::npos)
				aIter->mValue += aNewValue + FILTER_DELIMITER;
		}
	}
	else
	{
		aNewFilter.mFilterType	= aType;
		aNewFilter.mIsMatched	= aIsMatched;
		aNewFilter.mValue		= aValue;

		mRequestFilterList.push_back (aNewFilter);
	}
	/*----------------------------------------------------------------*/
}

void
CRXFilter::RemoveRequestFilter (const E_CRX_FILTER_REQUEST aType)
{
	CRXFilter_t::iterator	aIter = mRequestFilterList.begin ();

	/*----------------------------------------------------------------*/
	for ( ; aIter != mRequestFilterList.end () ; aIter++)
	{
		if (aIter->mFilterType == aType)
		{
			mRequestFilterList.erase (aIter);
			break;
		}
	}
	/*----------------------------------------------------------------*/
}

bool
CRXFilter::CheckRequestFilter (const E_CRX_FILTER_REQUEST	aType,
							   const CRXHttpRequest			& aRequest)
{
	size_t	aPosition	= std::string::npos;

	bool	aIsMatched	= false;
	bool	aIsFound	= false;

	CRXFilter_t::iterator	aIter = mRequestFilterList.begin ();

	/*----------------------------------------------------------------*/
	if (aRequest.GetFileExtension ().length () == 0)
		return false;

	for ( ; aIter != mRequestFilterList.end () ; aIter++)
	{
		if (aIter->mFilterType == aType)
		{
			aPosition	= aIter->mValue.find (aRequest.GetFileExtension ());
			aIsMatched	= aIter->mIsMatched;
			break;
		}
	}
	/*----------------------------------------------------------------*/

	aIsFound = aPosition == std::string::npos ? false : true;

	return aIsFound && aIsMatched;
}
