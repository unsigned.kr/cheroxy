/**
 * CRXProxy.cpp
 */

#include "CRXProxy.h"

#include <string.h>

CRXFilter	CRXProxy::mFilter;

CRXProxy::CRXProxy (const int aSocket)
	: mServerTimeout (0)
{
	/*----------------------------------------------------------------*/
	if (aSocket < 0)
		return ;

	mClient = aSocket;
	/*----------------------------------------------------------------*/
}

CRXProxy::~CRXProxy (void)
{
	/*----------------------------------------------------------------*/
	Close ();
	/*----------------------------------------------------------------*/
}

void
CRXProxy::SetServerTimeout (const int aTimeout)
{
	/*----------------------------------------------------------------*/
	mServerTimeout = aTimeout;
	/*----------------------------------------------------------------*/
}

void
CRXProxy::Close (void)
{
	/*----------------------------------------------------------------*/
	mServer.Close ();
	mClient.Close ();
	/*----------------------------------------------------------------*/
}

int
CRXProxy::Forward (void)
{
	int		aResult = 0;

	/*----------------------------------------------------------------*/
	aResult = ReceiveRequest ();
	if (aResult < 0)
	{
		aResult = ERROR_PROXY_FAILED_TO_RECEIVE_REQUEST;
		CRX_ERROR_SET (aResult, "Failed to receive from client.");
	}

	aResult = SendRequest ();
	if (aResult < 0)
	{
		aResult = ERROR_PROXY_FAILED_TO_SEND_REQUEST;
		CRX_ERROR_SET (aResult, "Failed to send to server.");
		return aResult;
	}

	aResult = ReceiveResponse ();
	if (aResult < 0)
	{
		aResult = ERROR_PROXY_FAILED_TO_RECEIVE_RESPONSE;
		CRX_ERROR_SET (aResult, "Failed to receive from server.");
	}

	aResult = SendResponse ();
	if (aResult < 0)
	{
		aResult = ERROR_PROXY_FAILED_TO_SEND_RESPONSE;
		CRX_ERROR_SET (aResult, "Failed to send to client.");
		return aResult;
	}
	/*----------------------------------------------------------------*/

	return aResult;
}

int
CRXProxy::ReceiveRequest (void)
{
	int			aResult = 0;
	char		aBuffer;

	std::string	aHttpHeader;

	/*----------------------------------------------------------------*/
	for (;;)
	{
		aResult = mClient.Receive (&aBuffer, 1);
		if (aResult == -1)
		{
			aResult = ERROR_PROXY_FAILED_TO_RECEIVE_REQUEST;
			CRX_ADD_SUBCLASS_ERROR (mClient);
			CRX_ERROR_SET (aResult, "Failed to receive from client.");
			return aResult;
		}
		aHttpHeader += aBuffer;
		if (aHttpHeader.rfind (CRLF2) != std::string::npos)
			break;
	}

	mHttpRequest.SetHeader (aHttpHeader.c_str ());
	/*----------------------------------------------------------------*/

	return aResult < 0 ? aResult : 0;
}

int
CRXProxy::SendRequest (void)
{
	int	aResult	= 0;
	int	aLength	= mHttpRequest.GetHeader ().length ();

	/*----------------------------------------------------------------*/
	aResult = mServer.Connect (mHttpRequest.GetHost (), mHttpRequest.GetPort (), mServerTimeout);
	if (aResult < 0)
	{
		aResult = ERROR_PROXY_FAILED_TO_CONNECT_TO_SERVER;
		CRX_ADD_SUBCLASS_ERROR (mServer);
		CRX_ERROR_SET (aResult, "Failed to connect to server <%s>.", mHttpRequest.GetURL ().c_str ());
		return aResult;
	}

	aResult = mServer.Send (mHttpRequest.GetHeader ().c_str (), aLength);
	if (aResult != aLength)
	{
		CRX_ADD_SUBCLASS_ERROR (mServer);
		CRX_ERROR_SET (aResult, "Failed to send to server <%s>.", mHttpRequest.GetURL ().c_str ());
	}
	/*----------------------------------------------------------------*/

	return aResult < 0 ? aResult : 0;
}

int
CRXProxy::ReceiveResponse (void)
{
	int			aResult = 0;
	char		aBuffer;

	std::string	aHttpHeader;
	bool		aRemainHeader = true;

	/*----------------------------------------------------------------*/
	for (;;)
	{
		aResult = mServer.Receive (&aBuffer, 1);
		if (aResult < 0)
		{
			aResult = ERROR_PROXY_FAILED_TO_RECEIVE_REQUEST;
			CRX_ADD_SUBCLASS_ERROR (mServer);
			CRX_ERROR_SET (aResult, "Failed to receive from server.");
			break;
		}
		else if (aResult == 0)
		{
			CRX_ADD_SUBCLASS_ERROR (mServer);
			CRX_ERROR_SET (aResult, "Receive zero-byte from server.");
			break;
		}

		if (aRemainHeader)
		{
			aHttpHeader += aBuffer;
			if (aHttpHeader.rfind (CRLF2) != std::string::npos)
			{
				mHttpResponse.SetHeader (aHttpHeader.c_str ());
				aRemainHeader = false;

				if (mHttpResponse.GetStatusCode () == 304)	break;
			}
		}
		else
		{
			aResult = mHttpResponse.SetContent (&aBuffer, aResult);
			if (aResult < 0)
			{
				aResult = ERROR_PROXY_FAILED_TO_SET_RESPONSE;
				CRX_ERROR_SET (aResult, "Failed to set response.");
				break;
			}
			else if (aResult == 0)
				break;
		}
	}
	/*----------------------------------------------------------------*/

	return aResult < 0 ? aResult : 0;
}

int
CRXProxy::SendResponse (void)
{
	int	aResult = 0;
	int	aLength = 0;

	/*----------------------------------------------------------------*/
	aLength = mHttpResponse.GetHeader ().length ();
	aResult = mClient.Send (mHttpResponse.GetHeader ().c_str (), aLength);
	if (aResult != aLength)
	{
		CRX_ADD_SUBCLASS_ERROR (mClient);
		CRX_ERROR_SET (aResult, "Failed to send to client.");
		return aResult;
	}

	aLength = mHttpResponse.GetContentLength ();
	aResult = mClient.Send (mHttpResponse.GetContentBody (), aLength);
	if (aResult != aLength)
	{
		CRX_ADD_SUBCLASS_ERROR (mClient);
		CRX_ERROR_SET (aResult, "Failed to send to client.");
	}
	/*----------------------------------------------------------------*/

	return aResult < 0 ? aResult : 0;
}

CRXFilter &
CRXProxy::GetFilter (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/

	return mFilter;
}

CRXHttpRequest &
CRXProxy::GetHttpRequest (void)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/

	return mHttpRequest;
}

CRXHttpResponse &
CRXProxy::GetHttpResponse (void)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/

	return mHttpResponse;
}
