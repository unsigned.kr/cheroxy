/**
 * CRXException.cpp
 */

#include "CRXException.h"

#include <sstream>

#ifdef _WIN32
# include <Windows.h>
#else
# include <errno.h>
#endif

CRXException::CRXException (void)
	: mCode (0)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
}

CRXException::~CRXException (void)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
}

int
CRXException::GetErrorCode (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
	return mCode;
}

std::string
CRXException::GetErrorMessage (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
	return mMessage;
}

int
CRXException::GetSystemErrorCode (void)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
#ifdef _WIN32
	return GetLastError ();
#else
	return errno;
#endif
}

void
CRXException::SetErrorCode (const int aCode)
{
	/*----------------------------------------------------------------*/
	mCode = aCode;
	/*----------------------------------------------------------------*/
}

void
CRXException::SetErrorMessage (const char	* aFile,
							   const char	* aFunction,
							   const int	aLine,
							   const char	* aFormat, ...)
{
	va_list aVaList;

	/*----------------------------------------------------------------*/
	va_start (aVaList, aFormat);

	mMessage += MakeErrorMessage (aFile, aFunction, aLine, aFormat, aVaList) + "\n";

	va_end (aVaList);
	/*----------------------------------------------------------------*/
}

std::string
CRXException::MakeErrorMessage (const char	* aFile,
								const char	* aFunction,
								const int	aLine,
								const char	* aFormat,
								va_list		aVaList)
{
	/*---------------------------------------------------------------
	 * local variables
	 *--------------------------------------------------------------*/
	double	aDoubleValue = .0;
	int		aIntValue = 0;
	char	*aStringValue = NULL;
	char	*aPtr = NULL;
	void *	aHexValue = NULL;

	std::stringstream	aErrorStream;

	/*----------------------------------------------------------------*/
#ifdef _DEBUG
	/*----------------------------------------------------------------
	 * 1. set default error message
	 *----------------------------------------------------------------*/
	aErrorStream << "[" << aFile << "]";
	if (aLine != 0)
		aErrorStream << "[" << aLine << "]";
	else
		aErrorStream << "[EOF]";
	aErrorStream << "[" << aFunction << "] ";
#endif

	/*----------------------------------------------------------------
	 * 2. set error string from arguments
	 *----------------------------------------------------------------*/
	for (aPtr = (char*)aFormat ; *aPtr ; ++aPtr)
	{
		if (*aPtr == '%')
		{
			switch (*++aPtr)
			{
				case 'f':   /* double value */
					aDoubleValue = va_arg(aVaList, double);
					aErrorStream << aDoubleValue;
					break;

				case 'd':
					aIntValue = va_arg(aVaList, int);
					aErrorStream << aIntValue;
					break;

				case 's':
					aStringValue = va_arg(aVaList, char *);
					if (aStringValue != NULL)
						aErrorStream << aStringValue;
					break;

				case 'p':
					aHexValue = va_arg(aVaList, void *);
					aErrorStream << std::hex << aHexValue;
					break;

				default:
					aErrorStream << *aPtr;
					break;
			}
		}
		else
			aErrorStream << *aPtr;
	}
	/*----------------------------------------------------------------*/

	return aErrorStream.str();
}

void
CRXException::AddMessage (const char * aMessage)
{
	mMessage += aMessage;
}
