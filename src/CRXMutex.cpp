
#include "CRXMutex.h"

CRXMutex::CRXMutex (void)
	: mIsCreated (false)
{
	/*----------------------------------------------------------------*/
#ifdef _WIN32
	  mMutex = NULL;
#endif
	/*----------------------------------------------------------------*/
}

CRXMutex::~CRXMutex (void)
{
	/*----------------------------------------------------------------*/
	this->Unlock ();
	this->Destroy ();
	/*----------------------------------------------------------------*/
}

bool
CRXMutex::IsCreated (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/

	return mIsCreated;
}

int
CRXMutex::Create (void)
{
	int		aResult = 0;

	/*----------------------------------------------------------------*/
#ifdef _WIN32
	if ((mMutex = ::CreateMutexA (NULL, FALSE, NULL)) == NULL)
		aResult = GetLastError ();
#else
	aResult = pthread_mutex_init (&mMutex, NULL);
#endif

	mIsCreated = true;
	/*----------------------------------------------------------------*/

	return aResult;
}

int
CRXMutex::Destroy (void)
{
	int		aResult = 0;

	/*----------------------------------------------------------------*/
#ifdef _WIN32
	if (::CloseHandle (mMutex))
		aResult = GetLastError ();
#else
	aResult = pthread_mutex_destroy (&mMutex);
#endif

	mIsCreated = false;
	/*----------------------------------------------------------------*/

	return aResult;
}

void
CRXMutex::Lock (void)
{
	/*----------------------------------------------------------------*/
#ifdef _WIN32
	::WaitForSingleObject (mMutex, INFINITE);
#else
	pthread_mutex_lock (&mMutex);
#endif
	/*----------------------------------------------------------------*/
}

void
CRXMutex::Unlock (void)
{
	/*----------------------------------------------------------------*/
#ifdef _WIN32
	if (mMutex != NULL)
		::ReleaseMutex(mMutex);
#else
	pthread_mutex_unlock (&mMutex);
#endif
	/*----------------------------------------------------------------*/
}
