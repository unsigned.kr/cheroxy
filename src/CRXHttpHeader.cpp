/**
 * CRXHttpHeader.cpp
 */
#ifdef _WIN32
# pragma warning (disable:4996)
#endif

#include "CRXHttpHeader.h"

CRXHttpHeader::CRXHttpHeader (void)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
}

CRXHttpHeader::~CRXHttpHeader (void)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
}

void
CRXHttpHeader::Set (const char * aHeader)
{
	/*----------------------------------------------------------------*/
	if (aHeader == NULL)
		return ;

	mHttpHeader.assign (aHeader);

	this->Parse ();
	/*----------------------------------------------------------------*/
}

std::string
CRXHttpHeader::Get (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/

	return mHttpHeader;
}
