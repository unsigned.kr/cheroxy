/**
 * CRXHttpRequest.cpp
 */
#ifdef _WIN32
# pragma warning (disable:4996)
#endif

#include "CRXHttpRequest.h"

#include <stdio.h>
#include <stdlib.h>

CRXHttpRequest::CRXHttpRequest (void)
	: mPort (0)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
}

CRXHttpRequest::~CRXHttpRequest (void)
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/
}

void
CRXHttpRequest::SetHeader (const char * aHeader)
{
	/*----------------------------------------------------------------*/
	Set (aHeader);
	/*----------------------------------------------------------------*/
}

std::string
CRXHttpRequest::GetHeader (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/

	return Get ();
}

void
CRXHttpRequest::Parse (void)
{
	std::string	aHttpRequestUrl;

	char		aMethod[64]			= {0x00, };
	char		aUrl[1024 * 4]		= {0x00, };
	char		aHttpVersion[64]	= {0x00, };

	char		aProtocol[32]		= {0x00, };
	char		aHostBuffer[512]	= {0x00, };
	char		aHost[512]			= {0x00, };
	char		aPortBuffer[32]		= {0x00, };
	int			aPort				= 0;

	/*----------------------------------------------------------------*/
	aHttpRequestUrl = GetHeader ().substr (0, mHttpHeader.find ('\r'));

	/*----------------------------------------------------------------
	 * 1. separate first line to <METHOD>, <URL> and <VERSION>
	 *----------------------------------------------------------------*/
	sscanf (aHttpRequestUrl.c_str (), "%[^ ] %[^ ] %[^ ]\r\n", aMethod, aUrl, aHttpVersion);
	mMethod.assign (aMethod);
	mUrl.assign (aUrl);
	mHttpVersion.assign (aHttpVersion);

	ParseFileExtension ();

	/*----------------------------------------------------------------
	 * 2. separate <URL> to <PROTOCOL> and <HOST>
	 *----------------------------------------------------------------*/
	sscanf (mUrl.c_str (), "%[^://]://%[^/]", aProtocol, aHostBuffer);
	mProtocol.assign (aProtocol);

	/*----------------------------------------------------------------
	 * 3. separate <HOST> to <HOSTNAME>:<PORT>
	 *----------------------------------------------------------------*/
	sscanf (aHostBuffer, "%[^:]:%s", aHost, aPortBuffer);
	mHost.assign (aHost);
	aPort = atoi (aPortBuffer);

	mPort = aPort == 0 ? 80 : aPort;
	/*----------------------------------------------------------------*/
}

void
CRXHttpRequest::ParseFileExtension (void)
{
	std::string	aFilename;

	size_t		aFilenameOffset		= 0;
	size_t		aParameterPosition	= 0;
	size_t		aSlashPosition		= 0;
	size_t		aDotPosition		= 0;

	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------
	 * 1. get filename
	 *----------------------------------------------------------------*/
	aParameterPosition = mUrl.find ('?');
	aSlashPosition = mUrl.rfind ('/', aParameterPosition) + 1;
	if (aSlashPosition == mUrl.length ())
		return ;

	aFilenameOffset	= (aParameterPosition != std::string::npos)
					? aParameterPosition - aSlashPosition
					: std::string::npos;

	aFilename = mUrl.substr (aSlashPosition, aFilenameOffset);

	/*----------------------------------------------------------------
	 * 2. get file extension
	 *----------------------------------------------------------------*/
	aDotPosition = aFilename.rfind ('.');
	if (aDotPosition != std::string::npos)
		mFileExtension = aFilename.substr (aDotPosition + 1);
	/*----------------------------------------------------------------*/
}

std::string
CRXHttpRequest::GetURL (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/

	return mUrl;
}

std::string
CRXHttpRequest::GetHost (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/

	return mHost;
}

int
CRXHttpRequest::GetPort (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/

	return mPort;
}

std::string
CRXHttpRequest::GetFileExtension (void) const
{
	/*----------------------------------------------------------------*/
	/*----------------------------------------------------------------*/

	return mFileExtension;
}
